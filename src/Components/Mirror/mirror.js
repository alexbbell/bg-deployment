import React from 'react'
import { useSelector  } from 'react-redux'


export default function Mirror() {

    const count = useSelector((state) => state.counter.value)

    return (
        <div>
            MY Mirror
            <b>{count}</b>
        </div>
    )

}