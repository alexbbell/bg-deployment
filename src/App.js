import './App.css';

import './Components/Counter/Counter';
import { Counter } from './Components/Counter/Counter';
import Mirror from './Components/Mirror/mirror';
import Navbar from './app/navbar';



function App() {
  return (
    
    //  <Router>
    
      <div className="App">
        <Navbar />
        <header className="App-header">

          <Mirror />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>

          <Counter />
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React 04 01 2023 17 24
          </a>
        </header>
      </div>
      //  </Router>

  );
}

export default App;
