import React from 'react';


export default function Navbar() {

    return (
        <>
            <nav>
                <ul>
                    <li>
                        <div>Home</div>
                    </li>
                    <li>
                        <div>About</div>
                        {/* <Link to="/about">About</Link> */}
                    </li>
                    <li>
                        <div>Dashboard</div>

                        {/* <Link to="/dashboard">Dashboard</Link> */}
                    </li>
                    <li>
                        <div>Nothing Here</div>
                        {/* <Link to="/nothing-here">Nothing Here</Link> */}
                    </li>
                </ul>
            </nav>

            <hr />
        </>

    )
}